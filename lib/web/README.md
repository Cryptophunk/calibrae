# Calibrae Web App

This app is available by default via configured RPC nodes and allows the direct funding of operation of RPC nodes via
the use of beneficiary splits going to the RPC node operator.

This application will be derived from (busy.org's web app, which is much nicer though not quite as complete)[https://github.com/busyorg/busy]. As well as from steemit.com. The reference material for our developers is available here: [https://gitlab.com/calibr.ae/busyfork](https://gitlab.com/calibr.ae/busyfork) and [https://gitlab.com/calibr.ae/shimmer](https://gitlab.com/calibr.ae/shimmer), and of course also will derive from steem-js, which has been replicated here: [https://gitlab.com/calibr.ae/shimmer](https://gitlab.com/calibr.ae/shimmer).