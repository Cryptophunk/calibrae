////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  cmd/calibrd/main.go                                                       //
//                                                                            //
//  The main server application for the Calibrae blockchain forum system      //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  Credits for creation of this code can be found in the Git log.            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  "LICENCE" for redistribution, derivation and use of this code             //
//                                                                            //
//  This is free and unencumbered software released into the public domain.   //
//                                                                            //
//  Anyone  is  free  to  copy,  modify,  publish,  use,  compile,  sell, or  //
//  distribute  this software,  either in source code  form or as a compiled  //
//  binary, for any purpose, commercial or non-commercial, and by any means.  //
//                                                                            //
//  In jurisdictions  that recognize  copyright laws,  the author or authors  //
//  of this software dedicate any and all copyright interest in the software  //
//  to the public  domain.   We make this dedication  for the benefit of the  //
//  public at  large and to  the detriment  of our heirs  and successors. We  //
//  intend  this  dedication  to  be   an  overt  act  of  relinquishment in  //
//  perpetuity  of  all present  and  future  rights to this  software under  //
//  copyright law.                                                            //
//                                                                            //
//  THE  SOFTWARE  IS  PROVIDED  "AS IS",  WITHOUT  WARRANTY  OF  ANY  KIND,  //
//  EXPRESS  OR  IMPLIED,  INCLUDING  BUT NOT LIMITED  TO  THE WARRANTIES OF  //
//  MERCHANTABILITY,   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  //
//  IN  NO EVENT  SHALL THE  AUTHORS BE LIABLE  FOR ANY  CLAIM,   DAMAGES OR  //
//  OTHER  LIABILITY,  WHETHER IN AN ACTION OF CONTRACT,  TORT OR OTHERWISE,  //
//  ARISING FROM,  OUT OF OR IN CONNECTION WITH  THE SOFTWARE  OR THE USE OR  //
//  OTHER DEALINGS IN THE SOFTWARE.                                           //
//                                                                            //
//  For more information, please refer to <http://unlicense.org/>             //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

package main

import (
	"flag"
	"fmt"
)

func main() {
	const appname = "calibrd"
	const version = "v0.0.1"
	const defaultCfgDir = ".calibrd"

	// parse commandline options
	helpPtr := flag.Bool("h", false, "Show CLI options help")
	verPtr := flag.Bool("v", false, "Show version")
	var cfgDir string
	flag.StringVar(&cfgDir, "d", ".calibrd", "Set calibrd config and data directory, default .calibrd")

	flag.Parse()

	if *helpPtr {
		// print help information
		fmt.Printf("Command line options for %s\n", appname)
		fmt.Println("-h - show help information")
		fmt.Println("-v - show version information")
		fmt.Printf("-d=<config directory> - set location of configuration directory (default=%s)\n", defaultCfgDir)
	} else if *verPtr {
		fmt.Printf("%s version %s\n", appname, version)
	} else {
		fmt.Println("Starting Calibrae server")

		// set configuration directory
		if cfgDir == defaultCfgDir {
			fmt.Printf("Using default config directory: '%s'\n", defaultCfgDir)
			cfgDir = defaultCfgDir
		} else {
			fmt.Printf("Using config directory %s\n", cfgDir)
		}
	}
}

func writeDefaultLoggingConfig() {
	return
}

func loadLoggingConfigFromIni() {
	return
}
